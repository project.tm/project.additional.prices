<?php

namespace Project\Additional\Prices;

class Utility {

    const IS_CACHE = false;
    const CACHE_TIME = 3600;
    const CACHE_DIR = '/wholesale.prices/';

    static public function useCache($cacheId, $func, $time = self::CACHE_TIME) {
        $obCache = new \CPHPCache;
        $cacheId = 'local:' . $time . ':' . (is_array($cacheId) ? implode(':', $cacheId) : $cacheId);
        if (self::IS_CACHE and $obCache->InitCache($time, $cacheId, self::CACHE_DIR)) {
            $arResult = $obCache->GetVars();
        } elseif ($obCache->StartDataCache()) {
            $arResult = $func();
            $obCache->EndDataCache($arResult);
        }
        return $arResult;
    }

}
