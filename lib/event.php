<?php

namespace Project\Additional\Prices;

class Event {

    static $is = false;

    static public function isOn() {
        if (self::$is) {
            return false;
        } else {
            self::$is = true;
            return true;
        }
    }

    static public function isOff() {
        self::$is = false;
    }

    static public function OnAfterUserAdd($arFields) {
        if (self::isOn()) {
            User::setGroup($arFields['ID']);
            self::isOff();
//            preExit($arFields);
        }
    }

    static public function OnAfterUserUpdate($arFields) {
        if (self::isOn()) {
            User::setGroup($arFields['ID']);
            self::isOff();
//            preExit($arFields);
        }
    }

}
