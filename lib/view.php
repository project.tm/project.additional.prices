<?php

namespace Project\Additional\Prices;

use CRZBitronic2Settings,
    CRZBitronic2CatalogUtils;

class View {

    static public function isWholesale() {
        return User::getGroup() ? 'wholesale' : 'retail';
    }

    static public function isAddation($arItemPrices) {
        return User::getGroup() or count($arItemPrices) > 1;
    }

    static public function isPriceView($arItem) {
//        if (User::getGroup() and ( count($arItem['PRICES']) == 1 and isset($arItem['PRICES']['BASE']))) {
//            return true;
//        }
        return false;
    }

    static public function additionalPrices($arItem, $sep = '') {
        if (User::getGroup() && CRZBitronic2Settings::isPro()) {
            $arData = Price::getData($arItem['IBLOCK_ID'], $arItem['ID']);
            ?>
            <div class="wrapper baron-wrapper additional-prices-wrap additional-prices-<?= View::isWholesale() ?>">
                <div class="scroller scroller_v"><?
                    foreach ($arData['PRICES'] as $priceCode => $arPrice) {
                        if ($priceCode != 'BASE' or empty($arPrice['DISCOUNT_VALUE']))
                            continue;
                        Price::filterOld($arItem, $arPrice);
                        $bDiscountShow = $old ? true : (0 < $arPrice['DISCOUNT_DIFF']);
                        ?>
                        <div class="additional-price-type">
                            <span class="price-desc"><?= $arData['CAT_PRICES'][$priceCode]['TITLE'] ?>:</span><?= $sep ?>
                            <span class="price"><?
                                if ($bDiscountShow) {
                                    if ($bDiscountShow):
                                        ?><span class="price-old"><?= CRZBitronic2CatalogUtils::getElementPriceFormat($arPrice['CURRENCY'], $arPrice['VALUE'], $arPrice['PRINT_VALUE']) ?></span><? endif ?><?
                                }
                                echo CRZBitronic2CatalogUtils::getElementPriceFormat(
                                        $arPrice['CURRENCY'], $arPrice['DISCOUNT_VALUE'], $arPrice['PRINT_DISCOUNT_VALUE']
                                );
                                ?>
                            </span>
                        </div>
                    <? } ?>
                    <div class="scroller__track scroller__track_v">
                        <div class="scroller__bar scroller__bar_v"></div>
                    </div>
                </div>
            </div>
            <?
        }
    }

}
