<?php

namespace Project\Additional\Prices;

use CIBlockElement,
    CIBlockPriceTools;

class Price {

    static private $priceCode = ['BASE'];
    static private $prices = [
        User::DEALER => 'DEALER',
        User::PARTNER => 'PARTNER',
        User::WHOLESALE => 'WHOLESALE',
    ];
    static private $pricesCodes = [
        1 => 'BASE',
        10 => 'DEALER',
        11 => 'PARTNER',
        12 => 'WHOLESALE',
    ];

    const PRICE_VAT_INCLUDE = true;

    static public function getCode() {
        return isset(self::$prices[User::getGroup()]) ? [self::$prices[User::getGroup()]] : self::$priceCode;
    }

    static public function getCodeById($ID) {
        return isset(self::$pricesCodes[$ID]) ? self::$pricesCodes[$ID] : 'BASE';
    }

    static public function filterOld($arItem, &$arPrice) {
        $code = self::getCodeById($arPrice['PRICE_ID']);
        $old = empty($arItem['PROPERTY_PRICE_' . $code . '_OLD_VALUE']) ? (empty($arItem['PROPERTIES']['PRICE_' . $code . '_OLD']['VALUE']) ? 0 : $arItem['PROPERTIES']['PRICE_' . $code . '_OLD']['VALUE']) : $arItem['PROPERTY_PRICE_' . $code . '_OLD_VALUE'];
        if ($old) {
            $arPrice['DISCOUNT_DIFF'] = $old - $arPrice['DISCOUNT_VALUE'];
            $arPrice['VALUE'] = $old;
            $arPrice['VALUE'] = $old;
        }
    }

    static public function getData($IBLOCK_ID, $ID, $priceCode = ['BASE']) {
        list($arResultPrices, $arResultPricesAllow) = Utility::useCache([__CLASS__, __FUNCTION__, '$arResultPrices ', User::getGroup(), $IBLOCK_ID, $priceCode], function() use ($IBLOCK_ID, $priceCode) {
                    $arResultPrices = CIBlockPriceTools::GetCatalogPrices($IBLOCK_ID, $priceCode);
                    $arResultPricesAllow = CIBlockPriceTools::GetAllowCatalogPrices($arResultPrices);
                    return [$arResultPrices, $arResultPricesAllow];
                });

        $arResult = Utility::useCache([__CLASS__, __FUNCTION__, '$arResult ', User::getGroup(), $arResultPrices, $IBLOCK_ID, $ID], function() use ($IBLOCK_ID, $ID, $arResultPrices, $arResultPricesAllow) {
                    $arConvertParams = [];
                    $arSelect = ['ID', 'IBLOCK_ID', 'NAME', 'PROPERTY_PRICE_BASE_OLD', 'PROPERTY_PRICE_DEALER_OLD', 'PROPERTY_PRICE_PARTNER_OLD', 'PROPERTY_PRICE_WHOLESALE_OLD'];
                    $arFilter = [
                        'IBLOCK_ID' => $IBLOCK_ID,
                        'ID' => $ID
                    ];
                    foreach ($arResultPrices as &$value) {
                        if (!$value['CAN_VIEW'] && !$value['CAN_BUY'])
                            continue;
                        $arSelect[] = $value["SELECT"];
                        $arFilter["CATALOG_SHOP_QUANTITY_" . $value["ID"]] = $arParams["SHOW_PRICE_COUNT"];
                    }
                    $arResult['CONVERT_CURRENCY'] = $arConvertParams;

                    $res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
                    $arResult = $res->Fetch();

                    $arResult["CAT_PRICES"] = $arResultPrices;
                    $arResult["PRICES_ALLOW"] = $arResultPricesAllow;
                    $arResult["PRICE_MATRIX"] = false;
                    $arResult["PRICES"] = CIBlockPriceTools::GetItemPrices($IBLOCK_ID, $arResultPrices, $arResult, self::PRICE_VAT_INCLUDE, $arConvertParams);
                    return $arResult;
                });
        return $arResult;
    }

}
