<?php

namespace Project\Additional\Prices;

class Property {

    static public $_systemProps = array(
        'PRICE_BASE_OLD',
        'PRICE_DEALER_OLD',
        'PRICE_PARTNER_OLD',
        'PRICE_WHOLESALE_OLD'
    );

    static public function filter(&$arParams) {
        foreach ($arParams as $key => $value) {
            if (in_array($value, self::$_systemProps)) {
                unset($arParams[$key]);
            }
        }
    }

}
