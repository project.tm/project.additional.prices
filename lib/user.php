<?php

namespace Project\Additional\Prices;

use CUser;

class User {

    const RETAIL = 17; //Розница
    const DEALER = 13; //Деалер
    const PARTNER = 14; //Партнер
    const WHOLESALE = 15; //Крыпный опт

    static private $wholesalePrice = array(
        self::DEALER,
        self::PARTNER,
        self::WHOLESALE,
    );

    static public function getGroup() {
        static $isSearch = false, $is = 0;
        if (empty($isSearch)) {
            $isSearch = true;
            global $USER;
            foreach ($USER->GetUserGroupArray() as $value) {
                if (in_array($value, self::$wholesalePrice)) {
                    $is = $value;
                    break;
                }
            }
        }
        return $is;
    }

    static public function setGroup($ID) {
        $isRetail = $isWholesale = false;
        foreach ($groups = CUser::GetUserGroup($ID) as $value) {
            if (in_array($value, self::$wholesalePrice)) {
                $isWholesale = true;
            }
            if ($value == self::RETAIL) {
                $isRetail = true;
            }
        }
        if ($isWholesale and $isRetail) {
            foreach ($groups as $key => $value) {
                if (self::RETAIL == $value) {
                    unset($groups[$key]);
                }
            }
            CUser::SetUserGroup($ID, $groups);
        } else if (empty($isWholesale) and empty($isRetail)) {
            $groups[] = self::RETAIL;
            CUser::SetUserGroup($ID, $groups);
        }
    }

}
