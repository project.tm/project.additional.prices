# Список дополнительные цен #

**catalog.section**
```
#!php
<div class="prices" id="<?=$arItemIDs['PRICE_CONTAINER']?>">
    <? $frame = $this->createFrame($arItemIDs['PRICE_CONTAINER'], false)->begin(CRZBitronic2Composite::insertCompositLoader()) ?>
    <div class="<? if(Project\Additional\Prices\View::isPriceView($arItem)) { ?>hidden<? } ?> <?=(empty($availableOnRequest)?'':' invisible')?>">
        <? Project\Additional\Prices\Price::filterOld($arItem, $arItem['MIN_PRICE']); ?>
        <span class="price-old" id="<?=$arItemIDs['OLD_PRICE']?>">
            <? if($arItem['MIN_PRICE']['DISCOUNT_DIFF'] > 0 && $arParams['SHOW_OLD_PRICE'] == 'Y'): ?>
                <?=CRZBitronic2CatalogUtils::getElementPriceFormat($arItem['MIN_PRICE']['CURRENCY'], $arItem['MIN_PRICE']['VALUE'], $arItem['MIN_PRICE']['PRINT_VALUE']);?>
            <? endif ?>
        </span>
        <span class="price" id="<?=$arItemIDs['PRICE']?>">
                <?=($arItem['bOffers']) ? GetMessage('BITRONIC2_BLOCKS_FROM') : ''?>
                <?=CRZBitronic2CatalogUtils::getElementPriceFormat($arItem['MIN_PRICE']['CURRENCY'], $arItem['MIN_PRICE']['DISCOUNT_VALUE'], $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE']);?>
        </span>
    </div>
    <div id="<?= $arItemIDs['PRICE_ADDITIONAL'] ?>" class="prices additional-price-container">
        <? Project\Additional\Prices\View::additionalPrices($arItem) ?>
    </div>
    <? $frame->end() ?>
</div>
```